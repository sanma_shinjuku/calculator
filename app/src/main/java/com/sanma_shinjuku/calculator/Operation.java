package com.sanma_shinjuku.calculator;

/**
 * Created by saito.tatsuro on 2015/12/02.
 */
public enum  Operation {
    PLUS   { double eval(double x, double y) { return x + y; } },
    MINUS  { double eval(double x, double y) { return x - y; } },
    TIMES  { double eval(double x, double y) { return x * y; } },
    DIVIDE { double eval(double x, double y) { return x / y; } };
    abstract double eval(double x, double y);
}
