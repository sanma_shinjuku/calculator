package com.sanma_shinjuku.calculator;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    //変数
    private Button button;
    private TextView textView;
    private  Calc calc;

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MainFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        calc = new Calc();
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_main, container, false);
        textView = (TextView)view.findViewById(R.id.form);
        //0から1キーまでの動き
        button = (Button)view.findViewById(R.id.key_0);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonNumber(Number.ZERO);
                textView.setText(calc.getResult());
            }
        });
        button = (Button)view.findViewById(R.id.key_1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonNumber(Number.ONE);
                textView.setText(calc.getResult());
            }
        });
        button = (Button)view.findViewById(R.id.key_2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonNumber(Number.TWO);
                textView.setText(calc.getResult());
            }
        });
        button = (Button)view.findViewById(R.id.key_3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonNumber(Number.THREE);
                textView.setText(calc.getResult());
            }
        });
        button = (Button)view.findViewById(R.id.key_4);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonNumber(Number.FOUR);
                textView.setText(calc.getResult());
            }
        });
        button = (Button)view.findViewById(R.id.key_5);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonNumber(Number.FIVE);
                textView.setText(calc.getResult());
            }
        });
        button = (Button)view.findViewById(R.id.key_6);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonNumber(Number.SIX);
                textView.setText(calc.getResult());
            }
        });
        button = (Button)view.findViewById(R.id.key_7);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonNumber(Number.SEVEN);
                textView.setText(calc.getResult());
            }
        });
        button = (Button)view.findViewById(R.id.key_8);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonNumber(Number.EIGHT);
                textView.setText(calc.getResult());
            }
        });
        button = (Button)view.findViewById(R.id.key_9);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonNumber(Number.NINE);
                textView.setText(calc.getResult());
            }
        });

        //クリアボタン
        button = (Button)view.findViewById(R.id.key_c);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonClear();
                textView.setText(calc.getResult());
            }
        });

        button = (Button)view.findViewById(R.id.key_ac);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonAllClear();
                textView.setText(calc.getResult());
            }
        });

        button = (Button)view.findViewById(R.id.key_division);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonOp(Operation.DIVIDE);
                textView.setText(calc.getResult());
            }
        });

        button = (Button)view.findViewById(R.id.key_multiply);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonOp(Operation.TIMES);
                textView.setText(calc.getResult());
            }
        });

        button = (Button)view.findViewById(R.id.plus);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonOp(Operation.PLUS);
                textView.setText(calc.getResult());
            }
        });

        button = (Button)view.findViewById(R.id.key_minus);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonOp(Operation.MINUS);
                textView.setText(calc.getResult());
            }
        });

        button = (Button)view.findViewById(R.id.key_dot);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               calc.onButtonNumber(Number.COMMA);
                textView.setText(calc.getResult());
            }
        });

        button = (Button)view.findViewById(R.id.key_equal);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calc.onButtonEquale();
                textView.setText(calc.getResult());
            }
        });

        button = (Button)view.findViewById(R.id.key_percent);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateTranslationX((ImageView)view.findViewById(R.id.sanma));
            }
        });

        return view;
    }


    private void animateTranslationX( final ImageView target ) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat( target, "translationX", 200f, 0f );
        target.setVisibility(target.VISIBLE);
        // 3秒かけて実行させます
        objectAnimator.setDuration( 2000 );
        // アニメーションを開始します
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                target.setVisibility(target.INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                target.setVisibility(target.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        objectAnimator.start();
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
